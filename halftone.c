#define GL_GLEXT_PROTOTYPES
#define EGL_EGLEXT_PROTOTYPES

#define _GNU_SOURCE

#include <gbm.h>
#include <math.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <GLES3/gl31.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <drm.h>
#include <xf86drmMode.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <arpa/inet.h>

GLint local_workgroup_size[3];

const int input_w = 703;
const int input_h = 463;

uint8_t *
read_input()
{
    FILE *fp;

    uint8_t *ptr = malloc(input_w * input_h * 4);

    fp = fopen("input.bin", "r");
    fread(ptr, input_w * input_h * 4, 1, fp);
    fclose(fp);

    return ptr;
}

static char* read_file(const char *restrict filename, size_t *size)
{
    FILE *file = fopen(filename, "rb");
    int ret;

    if (!file) {
        perror("fopen");
        exit(1);
    }
    ret = fseek(file, 0, SEEK_END);
    if (ret) {
        perror("fseek");
        exit(1);
    }
    long fsize = ftell(file);
    if (fsize < 0) {
        perror("ftell");
        exit(1);
    }
    rewind(file);

    char *buf = malloc((size_t)fsize);
    *size = fread(buf, 1, (size_t)fsize, file);
    assert(*size == (size_t)fsize);
    fclose(file);

    return buf;
}

void
init(int width_px, int height)
{
   GLuint f, program;
   const char *p;
   char msg[512];
   GLint stat;
   GLuint index = 0;
   GLuint buffer_id = 0;
   size_t file_size = 0;
   GLint g_file_size;

   /* Compile the compute shader */
   p = read_file("compute.glsl", &file_size);
   g_file_size = file_size;
   f = glCreateShader(GL_COMPUTE_SHADER);
   glShaderSource(f, 1, &p, &g_file_size);
   glCompileShader(f);
   glGetShaderInfoLog(f, sizeof msg, NULL, msg);
   printf("fragment shader info: %s\n", msg);
   glGetShaderiv(f, GL_COMPILE_STATUS, &stat);
   if (!stat) {
      printf("Error: fragment shader did not compile!\n");
      exit(1);
   }

   /* Create and link the shader program */
   program = glCreateProgram();
   glAttachShader(program, f);

   glLinkProgram(program);
   glGetProgramInfoLog(program, sizeof msg, NULL, msg);
   printf("info: %s\n", msg);

   /* Enable the shaders */
   glUseProgram(program);

   glGetProgramiv(program, GL_COMPUTE_WORK_GROUP_SIZE, local_workgroup_size);

   /* Set uniforms */
   index = glGetUniformLocation(program, "input_w");
   glUniform1ui(index, input_w);
   index = glGetUniformLocation(program, "input_h");
   glUniform1ui(index, input_h);

   printf("SRW %d %d\n", __LINE__, glGetError());
   /* Bind input/output buffers */
   GLint binding_point = -1;
   glGenBuffers(1, &buffer_id);
   glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer_id);
   glBufferData(GL_SHADER_STORAGE_BUFFER, width_px * height * 4, read_input(), GL_STREAM_COPY);
   index = glGetProgramResourceIndex(program, GL_SHADER_STORAGE_BLOCK, "srcBuffer");
   GLenum prop = GL_BUFFER_BINDING;
   glGetProgramResourceiv(program, GL_SHADER_STORAGE_BLOCK, index, 1, &prop, sizeof(binding_point), NULL, &binding_point);
   printf("inbuf %d\n", binding_point);
   printf("SRW %d %d\n", __LINE__, glGetError());
   glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding_point, buffer_id);
   printf("SRW %d %d\n", __LINE__, glGetError());

   glGenBuffers(1, &buffer_id);
   glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer_id);
   glBufferData(GL_SHADER_STORAGE_BUFFER, width_px * height * 4, NULL, GL_STREAM_COPY);
   index = glGetProgramResourceIndex(program, GL_SHADER_STORAGE_BLOCK, "destBuffer");
   glGetProgramResourceiv(program, GL_SHADER_STORAGE_BLOCK, index, 1, &prop, sizeof(binding_point), NULL, &binding_point);
   printf("outbuf %d\n", binding_point);
   glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding_point, buffer_id);
   printf("SRW %d %d\n", __LINE__, glGetError());
}

EGLSurface egl_surf;
EGLContext egl_ctx;
EGLDisplay egl_dpy;

int
main(int argc, char *argv[])
{
   static const EGLint attribs[] = {
      EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
      EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
      EGL_NONE
   };
   static const EGLint ctx_attribs[] = {
      EGL_CONTEXT_CLIENT_VERSION, 2,
      EGL_NONE
   };
   EGLConfig config = 0;
   EGLint num_configs;
   EGLint egl_major, egl_minor;

   (void)argc;
   (void)argv;

   int fd = open("/dev/dri/renderD128", O_RDWR);
   struct gbm_device *gbm = gbm_create_device(fd);

   egl_dpy = eglGetDisplay(gbm);
   if (!egl_dpy) {
      printf("Error: eglGetDisplay() failed\n");
      return -1;
   }

   if (!eglInitialize(egl_dpy, &egl_major, &egl_minor)) {
      printf("Error: eglInitialize() failed\n");
      return -1;
   }

   eglBindAPI(EGL_OPENGL_ES_API);
   assert (eglQueryAPI() == EGL_OPENGL_ES_API);

   if (!eglChooseConfig( egl_dpy, attribs, &config, 1, &num_configs)) {
      printf("Error: couldn't get an EGL visual config\n");
      exit(1);
   }

   assert(config);
   assert(num_configs > 0);

   egl_surf = eglCreateWindowSurface(egl_dpy, config, (EGLNativeWindowType)NULL, NULL);

   egl_ctx = eglCreateContext(egl_dpy, config, NULL, ctx_attribs );
   if (egl_ctx == EGL_NO_CONTEXT) {
      printf("Error: eglCreateContext failed %x\n", eglGetError());
      exit(1);
   }

   if (!eglMakeCurrent(egl_dpy, egl_surf, egl_surf, egl_ctx)) {
      printf("Error: eglMakeCurrent() failed\n");
      return -1;
   }

   init(input_w, input_h);

   printf("local size %d %d %d\n",
	  local_workgroup_size[0],
	  local_workgroup_size[1],
	  local_workgroup_size[2]);

   for (int i=0; i<1; i++) {
      GLuint *data = NULL;
      FILE *fp;

      //printf("SRW %d %d\n", __LINE__, glGetError());
      //glDispatchCompute(input_w / local_workgroup_size[0], input_h / local_workgroup_size[1], 1);
      glDispatchCompute(1, 1, 1);
      //printf("SRW %d %d\n", __LINE__, glGetError());

      data = glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, input_w * input_h * 4, GL_MAP_READ_BIT);
      if (!data) {
	   printf("SRW %d %d\n", __LINE__, glGetError());
	   exit(1);
      }

      // convert -size 703x463 -depth 32 gray:frame.bin frame.png
      fp = fopen("frame.bin", "w");
      fwrite(data, input_w * input_h * 4, 1, fp);
      fclose(fp);

      glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
   }

   printf("SRW %d %d\n", __LINE__, glGetError());
   glFinish();
   printf("SRW %d %d\n", __LINE__, glGetError());

   eglMakeCurrent(egl_dpy, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
   eglDestroyContext(egl_dpy, egl_ctx);
   eglTerminate(egl_dpy);
   return 0;
}
