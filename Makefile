LDLIBS += -lEGL -lGLESv2 -lm
CFLAGS += `pkg-config --cflags libdrm` -g -Wall -Wextra -O2
LDLIBS += `pkg-config --libs libdrm gbm`

all: halftone

clean:
	rm -f halftone
	rm -f *.o
