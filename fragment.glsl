precision highp float;
uniform sampler2D input_data;
uniform sampler2D screen;
uniform float screen_w;
uniform float screen_h;
uniform float input_w;
uniform float input_h;

float threshold(float inp, vec4 thresh) {
    int result = 0;
    if (inp > thresh.r) {
        result += 1;
    }
    if (inp > thresh.g) {
        result += 2;
    }
    if (inp > thresh.b) {
        result += 4;
    }
    if (inp > thresh.a) {
        result += 8;
    }
    return float(result)/15.0;
}

void main(void)
{
    vec2 tex_coord = gl_FragCoord.xy;
    tex_coord.x = tex_coord.x / input_w;
    tex_coord.y = tex_coord.y / input_h;
    vec4 src_px = texture2D(input_data, tex_coord);

    vec2 screen_coord = gl_FragCoord.xy;
    screen_coord.x = fract(screen_coord.x * 4.0 / screen_w);
    screen_coord.y = fract(screen_coord.y / screen_h);
    vec4 threshes = texture2D(screen, screen_coord);
    gl_FragColor.r = threshold(src_px.r, threshes);
    screen_coord.x = fract((gl_FragCoord.x * 4.0 + 1.0) / screen_w);
    threshes = texture2D(screen, screen_coord);
    gl_FragColor.g = threshold(src_px.g, threshes);
    screen_coord.x = fract((gl_FragCoord.x * 4.0 + 2.0) / screen_w);
    threshes = texture2D(screen, screen_coord);
    gl_FragColor.b = threshold(src_px.b, threshes);
    screen_coord.x = fract((gl_FragCoord.x * 4.0 + 3.0) / screen_w);
    threshes = texture2D(screen, screen_coord);
    gl_FragColor.a = threshold(src_px.a, threshes);
}
