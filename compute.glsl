#version 310 es

layout(std430, binding = 1) buffer srcBuffer {
	float in_data[];
} inBuffer;

layout(std430, binding = 0) buffer destBuffer {
	uint data[];
} outBuffer;

layout (local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

uniform uint input_w;
uniform uint input_h;

void main()
{
	/* Convert the input image to f32 in place. Normally you'd read from an image buffer */
	for (uint x=0u; x<input_w; x++) {
		for (uint y=0u; y<input_h; y++) {
			uint offset = y * input_w + x;
			uint old_px = floatBitsToUint(inBuffer.in_data[offset]);
			// Take the most significant byte
			inBuffer.in_data[offset] = unpackUnorm4x8(old_px)[3];
		}
	}

	for (uint x=0u; x<input_w; x++) {
		for (uint y=0u; y<input_h; y++) {
			uint offset = y * input_w + x;
			float old_px = inBuffer.in_data[offset];

			// Threshold
			float new_px = old_px > 0.5 ? 1.0 : 0.0;

			outBuffer.data[offset] = packUnorm4x8(vec4(new_px, new_px, new_px, new_px));

			float error = old_px - new_px;

			inBuffer.in_data[offset + 1u] += error * 7.0 / 16.0;
			inBuffer.in_data[offset + input_w - 1u] += error * 3.0 / 16.0;
			inBuffer.in_data[offset + input_w + 0u] += error * 5.0 / 16.0;
			inBuffer.in_data[offset + input_w + 1u] += error * 1.0 / 16.0;
		}
	}
}
